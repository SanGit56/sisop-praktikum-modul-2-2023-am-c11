#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <time.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>

void exe_file_kill(char nama_file[]) {
    pid_t child_id;
    child_id = fork();

    if (child_id < 0) {
        printf("fork gagal!\n");
        exit(1);
    }

    if (child_id == 0) {
        char *argv[] = {"gcc", nama_file, "-o", "progkill", NULL};
        execv("/bin/gcc", argv);
    }
    
    int status;
    wait(&status);
}

void buat_file_kill(int pid_utama, char file_proses[], char mode_run[]) {
    FILE *fp;
    char nama_file[] = "progkill.c";

    fp = fopen(nama_file, "w");

    fprintf(fp, "#include <stdio.h>\n#include <stdlib.h>\n#include <signal.h>\n");
    fprintf(fp, "#include <unistd.h>\n#include <sys/types.h>\n#include <sys/wait.h>\n\n");

    fprintf(fp, "void hapus_program(char nama_file[]) {\n\tpid_t child_id;\n\tchild_id = fork();\n\n");
    fprintf(fp, "\tif (child_id < 0) {\n\t\tprintf(\"fork gagal!\");\n\t\texit(1);\n\t}\n\n");
    fprintf(fp, "\tif (child_id == 0) {\n\t\tchar *argv[] = {\"rm\", nama_file, NULL};\n\t\texecv(\"/bin/rm\", argv);\n\t}\n\n");
    fprintf(fp, "\tint status;\n\twait(&status);\n}\n\n");

    if (strcmp(mode_run, "-a") == 0) {
        fprintf(fp, "void pkill(char file_proses[]) {\n\tpid_t child_id;\n\tchild_id = fork();\n\n");
        fprintf(fp, "\tif (child_id < 0) {\n\t\tprintf(\"fork gagal!\");\n\t\texit(1);\n\t}\n\n");
        fprintf(fp, "\tif (child_id == 0) {\n\t\tchar *argv[] = {\"pkill\", \"-9\", \"-f\", file_proses, NULL};\n\t\texecv(\"/bin/pkill\", argv);\n\t}\n\n");
        fprintf(fp, "\twaitpid(child_id, NULL, 0);\n}\n\n");

        fprintf(fp, "int main() {\n\tpkill(\"%s\");\n\n", file_proses);
        fprintf(fp, "\thapus_program(\"progkill\");\n\t//hapus_program(\"progkill.c\");\n\n\treturn 0;\n}");
    } else if (strcmp(mode_run, "-b") == 0) {
        fprintf(fp, "int main() {\n\tint pid = %d;\n\tint terminate = kill(pid, SIGKILL);\n\n", pid_utama);
        fprintf(fp, "\thapus_program(\"progkill\");\n\t//hapus_program(\"progkill.c\");\n\n\treturn 0;\n}");
    }

    fclose(fp);

    exe_file_kill(nama_file);
}

void kompres(char nama_file[]) {
    pid_t child_id;
    child_id = fork();

    if (child_id < 0) {
        printf("fork gagal!\n");
        exit(1);
    }

    if (child_id == 0) {
        char nama_dir[25];
        strcpy(nama_dir, nama_file);
        strcat(nama_file, ".zip");

        char *argv[] = {"zip", "-9rmq", nama_file, nama_dir, NULL};
        execv("/bin/zip", argv);
    }
}

void unduh_gambar(char tautan[], char nama_dir[]) {
    pid_t child_id;
    child_id = fork();

    char nama_folder[60];
    strcpy(nama_folder, nama_dir);

    if (child_id < 0) {
        printf("fork gagal!\n");
        exit(1);
    }

    if (child_id == 0) {
        time_t waktu_dlm_dtk = time(NULL);
        struct tm* waktu_detail = localtime(&waktu_dlm_dtk);

        char nama_file[25];
        int ukuran = (waktu_dlm_dtk%1000)+50;
        char str_ukuran[5];

        snprintf(str_ukuran, 5, "%d", ukuran);
        strcat(tautan, str_ukuran);
        strftime(nama_file, 25, "/%Y-%m-%d_%H:%M:%S.jpg", waktu_detail);
        strcat(nama_folder, nama_file);
        
        char *argv[] = {"wget", "-qO", nama_folder, tautan, NULL};
        execv("/bin/wget", argv);
    }
}

void buat_dir(char tautan[], char nama_dir[]) {
    pid_t child_id;
    child_id = fork();

    if (child_id < 0) {
        printf("fork gagal!\n");
        exit(1);
    }

    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", nama_dir, NULL};
        execv("/bin/mkdir", argv);
    }
    
    waitpid(child_id, NULL, 0);

    pid_t gcid;
    gcid = fork();

    if (gcid < 0) {
        printf("fork gagal!\n");
        exit(1);
    }

    if (gcid == 0) {
        int i = 0;

        while(1) {
            if(i==15) {
                kompres(nama_dir);
                break;
            }
            
            unduh_gambar(tautan, nama_dir);

            i++;
            sleep(5);
        }

        exit(0);
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Berikan argumen untuk mode!\n");
        return 1;
    }

    // membuat proses
    pid_t pid, sid;
    pid = fork();
    char tautan[] = "https://picsum.photos/";

    if (pid > 0) {
        // buat program "killer"
        buat_file_kill(pid, argv[0], argv[1]);

        exit(EXIT_SUCCESS);
    }

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    umask(0);
    sid = setsid();

    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1) {
        // mengambil waktu untuk nama folder
        time_t waktu_dlm_dtk = time(NULL);
        struct tm* waktu_detail = localtime(&waktu_dlm_dtk);
        char nama_folder[25];
        strftime(nama_folder, 25, "%Y-%m-%d_%H:%M:%S", waktu_detail);
        
        buat_dir(tautan, nama_folder);
        sleep(30);
    }

    return 0;
}