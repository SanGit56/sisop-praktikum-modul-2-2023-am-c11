#include <ctype.h>     // isdigit()
#include <dirent.h>    // mkdir()
#include <stdbool.h>   // true / false
#include <stdio.h>     // printf
#include <stdlib.h>    // exit
#include <string.h>    // strcpy
#include <sys/wait.h>  // wait()
#include <time.h>      // srand(time(NULL))
#include <unistd.h>    // fork()

// ? function for printing error based on code
void print_error(int code) {
    if (code == 0)
        printf("Input not valid\n");
    else if (code == 1)
        printf("Could not open directory\n");
    else if (code == 2)
        printf("File not found\n");
    exit(1);
}

// ? function for checking if a string is a number
bool check_num(char *str) {
    while (*str)
        if (isdigit(*str++) == 0) return false;
    return true;
}

// ? function fot getting the index of the last '/' in a string
int last_slash(char *str) {
    int index = 0, last = 0;
    while (*str++) {
        index++;
        if (*str == '/') last = index;
    }
    return last;
}

// ? function got checking the argument
int check_argv(char str[], int code) {
    bool isInt = check_num(str);

    // ? checking hour (argv[1])
    if (code == 0) {
        if (isInt) {
            int hour = atoi(str);
            if (hour < 0 || hour > 23) print_error(0);
            printf("hour : %s\n", str);
            return hour;
        } else {
            if (!strcmp(str, "*")) {
                printf("hour : *\n");
                return -1;
            } else
                print_error(0);
        }
    }

    // ? checking minute (argv[2])
    else if (code == 1) {
        if (isInt) {
            int minute = atoi(str);
            if (minute < 0 || minute > 59) print_error(0);
            printf("minute : %s\n", str);
            return minute;
        } else {
            if (!strcmp(str, "*")) {
                printf("minute : *\n");
                return -1;
            } else
                print_error(0);
        }

    // ? checking second (argv[3])
    } else if (code == 2) {
        if (isInt) {
            int second = atoi(str);
            if (second < 0 || second > 59) print_error(0);
            printf("second : %s\n", str);
            return second;
        } else {
            if (!strcmp(str, "*")) {
                printf("second : *\n");
                return -1;
            }
            else
                print_error(0);
        }

    // ? checking directory (argv[4])
    } else if (code == 3) {
        char directory[strlen(str)];
        strcpy(directory, str);
        int last_slash_index = last_slash(str);

        // ? getting the filename (string after the last '/')
        // ? if '/' not exist, it will return null
        char *filename = strstr(str + last_slash_index, "/");
        if (filename == NULL) {
            // ? if '/' not exist, it means that the str
            // ? itself already the name of the file
            filename = str;
            // ? which means the directory is '.'
            strcpy(directory, ".");
        } else {
            // ? if '/' exist, the filename will include the '/'
            // ? so we need to increment the pointer
            filename++;
            // ? the directory is before the '/'
            // ? we can just cut it off by changing index after
            // ? the last '/' with '\0'
            directory[last_slash_index] = '\0';
        }

        // printf("%s\n", directory);
        // ? checking file
        DIR *d;
        struct dirent *dir;
        d = opendir(directory);
        // ? if d is not null, the directory is exist
        if (d) {
            bool fileExist = false;
            while ((dir = readdir(d)) != NULL) {
                char *f = dir->d_name;
                // ? checking if the file is inside the directory
                if (!strcmp(f, filename)) {
                    printf("directory : %s\n", dir -> d_name);
                    return 1;
                }
            }
            print_error(2);
        } else
            print_error(1);
    }
    return -1;
}

// ? function for running .sh file
void run(char dir[]){
    pid_t child_id;
    child_id = fork();

    if (child_id < 0) {
        printf("fork failed!\n");
        exit(1);
    }

    if (child_id == 0){
        char *argv[] = {"bash", dir, NULL};
        execv("/bin/bash", argv);
    }
    int status;
    wait(&status);
}

int main(int argc, char *argv[]) {
    char *directory;
    int hour, minute, second;
    if (argc != 5) {
        printf("number of arguments is not 5!\nargc : %d\n", argc);
        return 1;
    }

    else {
        hour = check_argv(argv[1], 0);
        minute = check_argv(argv[2], 1);
        second = check_argv(argv[3], 2);
        directory = argv[4];
        check_argv(argv[4], 3);
    }

    pid_t process_id = 0;
    process_id = fork();

    if (process_id < 0) {
        printf("fork failed!\n");
        exit(1);
    }

    // ? kill parent process
    if (process_id > 0) {
        printf("process_id of child process %d \n", process_id);
        exit(0);
    }

    while (1) {
        time_t now = time(NULL);
        struct tm *waktu_detail = localtime(&now);
        char temp[5];
        int jam, menit, detik;

        // ? getting current hour
        strftime(temp, 25, "%H", waktu_detail);
        jam = atoi(temp);

        // ? getting current minute
        strftime(temp, 25, "%M", waktu_detail);
        menit = atoi(temp);

        // ? getting current second
        strftime(temp, 25, "%S", waktu_detail);
        detik = atoi(temp);

        if ((hour == jam || hour == -1) && 
            (minute == menit || minute == -1) &&
            (second == detik || second == -1)) {
                run(directory);
            }
        sleep(1);
    }

    return 0;
}

/*
example:
    ./program \* 44 5 /home/Banabil/programcron.sh

explanation:
    \* = jam
    44 = menit
    5 = detik
*/
