#include <stdio.h>      // printf
#include <string.h>     // strcpy
#include <stdlib.h>     // exit
#include <dirent.h>     // mkdir()
#include <unistd.h>     // fork()
#include <sys/wait.h>   // wait()
#include <stdbool.h>    // true / false
#include <time.h>       // srand(time(NULL))

#define max_size 255

// ? fungsi melakukan wget
void download(char url[], char filename[]){
    pid_t child_id;
    child_id = fork();

    if (child_id < 0) {
        printf("fork failed!\n");
        exit(1);
    }

    if (child_id == 0){
        char *argv[] = {"wget", "-q", url, "-O", filename, NULL};
        execv("/bin/wget", argv);
    }
    int status;
    wait(&status);
}

// ? fungsi melakukan unzip file
void unzip(char filename[]){
    pid_t child_id;
    child_id = fork();

    if (child_id < 0) {
        printf("fork failed!\n");
        exit(1);
    }

    if (child_id == 0){
        char *argv[] = {"unzip", "-q", filename, NULL};
        execv("/bin/unzip", argv);
    }
    int status;
    wait(&status);
}

// ? fungsi menghapus file
void rm(char filename[]){
    pid_t child_id;
    child_id = fork();

    if (child_id < 0) {
        printf("fork failed!\n");
        exit(1);
    }

    if (child_id == 0){
        char *argv[] = {"rm", filename, NULL};
        execv("/bin/rm", argv);
    }
    int status;
    wait(&status);
}

// ? fungsi membuat direktori
void make_directory(char dirname[]){
    pid_t child_id;
    child_id = fork();

    if (child_id < 0) {
        printf("fork failed!\n");
        exit(1);
    }

    if (child_id == 0){
        char *argv[] = {"mkdir", "-p", dirname, NULL};
        execv("/bin/mkdir", argv);
    }
    int status;
    wait(&status);
}

// ? melakukan cek substring
bool extension_check(char filename[], char extension[]){
    if (strstr(filename, extension) != NULL)
        return true;
    else return false;
}

// ? ls | grep extension > list
int ls_grep(char file_list[][max_size], char extension[]){
    int count = 0;
    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            char *filename = dir -> d_name;
            if (extension_check(filename, extension)){
                strcpy(file_list[count], filename);
                count++;
            }
            // printf("%s\n", dir -> d_name);
        }
        closedir(d);
    }
    return count;
}

// ? fungsi untuk memindahkan file
void mv(char filename[][max_size], char dirname[], int size){
    for (int i = 0; i < size; i++){
        pid_t child_id;
        child_id = fork();

        if (child_id < 0) {
            printf("fork failed!\n");
            exit(1);
        }

        if (child_id == 0){
            char *argv[] = {"mv", filename[i], dirname, NULL};
            execv("/bin/mv", argv);
        }
        int status;
        wait(&status);
    }
    int status;
    wait(&status);
}

// ? fungsi untuk memotong string
void cut_until(char string[], char until){
    for (int i = 0; i < strlen(string); i++)
        if (string[i] == until){
            string[i] = '\0';
            break;
        }
}

// ? fungsi untuk melakukan zip
void zip(char file_list[][max_size], char filename[], int file_count){
    pid_t child_id;
    child_id = fork();

    if (child_id < 0) {
        printf("fork failed!\n");
        exit(1);
    }

    if (child_id == 0){
        int count = 0;
        char *argv[max_size];
        argv[count++] = "zip";
        argv[count++] = "-9rmq";
        argv[count++] = filename;
        for (int i = 0; i < file_count; i++)
            argv[count++] = file_list[i];    
        argv[count++] = NULL;    
        
        execv("/bin/zip", argv);
    }
    int status;
    wait(&status);
}

int main() {
    srand(time(NULL));
    char url[] = "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq";
    char filename[] = "Animals.zip";
    char dirname[][50] = {"HewanDarat", "HewanAmphibi", "HewanAir"};
    int dirname_len = sizeof(dirname) / sizeof(dirname[0]);
    char animal_type[][50] = {"darat", "amphibi", "air"};
    int animal_type_len = sizeof(dirname) / sizeof(dirname[0]);

    download(url, filename); // ? download zip pada url

    unzip(filename); // ? unzip zip file

    rm(filename);    // ? remove zip file
    
    // ? membuat directory
    for (int i = 0; i < dirname_len; i++)
        make_directory(dirname[i]);

    // ? file_list untuk menyimpan hasil ls
    // ? selected_animal untuk menyimpan nama hewan random yang akan dijaga Grep-kun
    // ? file_count untuk menghitung file dengan ekstensi .jpg
    char file_list[max_size][max_size], selected_animal[max_size];
    int file_count = ls_grep(file_list, ".jpg");
    int random_index = rand() % file_count;

    // ? meng-copy hewan yang terpilih ke dalam selected_animal
    strcpy(selected_animal, file_list[random_index]);

    // ? menghapus '_' dari isi selected_animal
    cut_until(selected_animal, '_');

    printf("melakukan shift penjagaan pada : %s\n", selected_animal);

    // ? memindahkan .jpg ke dalam directory sesuai jenisnya
    for (int i = 0; i < animal_type_len; i++){
        int animal_count = ls_grep(file_list, animal_type[i]);
        mv(file_list, dirname[i], animal_count);
    }

    // ? melakukan list directory "Hewan*" kemudian di zip
    file_count = ls_grep(file_list, "Hewan");
    zip(file_list, filename, file_count);


    return 0;
}
