#include <stdio.h>      
#include <string.h>     
#include <stdlib.h>     
#include <dirent.h>    
#include <unistd.h>     
#include <sys/wait.h>   
#include <stdbool.h>    
#include <time.h>    
#include <pwd.h>
#include <sys/stat.h>
#include <sys/types.h>

#define max_size 255 // Define Max Size = 255


void downloadDB (char *url, char *filename){ // Fungsi Untuk mendownload Database Pemain

    pid_t child; // Deklarasi PID -> child
    child = fork(); // Menyimpan PID child dengan sistem fork () 
    int status; // Deklarasi int -> status

    if (child < 0) { // Jika child < 0
        printf("fork failed!\n"); // Print proses fork () gagal
        exit(1); // Keluar dari if statement
    }

    if (child == 0){ // Jika child = 0
        char *argv[] = {"wget", "-q", url, "-O", filename, NULL}; // Deklarasi char -> argv, dengan argumen (unduh, quiet, url, output, nama file, akhir array)
        execv("/bin/wget", argv); // Memanggil fungsi sistem yang menjalankan "/bin/wget", dengan menggunakan daftar argumen dalam argv
    }
    
    wait(&status); // Menghentikan Parent Process dan menunggu child process selesai di eksekusi serta menyimpan output t/f ke status
}

void unzip(char *filename){ // Fungsi Untuk unzip file

    pid_t child; // Deklarasi PID -> child
    child = fork(); // Menyimpan PID child dengan sistem fork () 
    int status; // Deklarasi int -> status

    if (child < 0) { // Jika child < 0
        printf("fork failed!\n"); // Print proses fork () gagal
        exit(1); // Keluar dari if statement
    }

    if (child == 0){ // Jika child = 0
        char *argv[] = {"unzip", "-q", filename, NULL}; // Deklarasi char -> argv, dengan argumen (unzip, quiet, nama file, akhir array)
        execv("/bin/unzip", argv); // Memanggil fungsi sistem yang menjalankan "/bin/unzip", dengan menggunakan daftar argumen dalam argv
    }

    wait(&status); // Menghentikan Parent Process dan menunggu child process selesai di eksekusi serta menyimpan output t/f ke status
}

void rm(char *filename){ // Fungsi Untuk remove file

    pid_t child; // Deklarasi PID -> child
    child = fork(); // Menyimpan PID child dengan sistem fork () 
    int status; // Deklarasi int -> status

    if (child < 0) { // Jika child < 0
        printf("fork failed!\n"); // Print proses fork () gagal
        exit(1); // Keluar dari if statement
    }

    if (child == 0){ // Jika child = 0
        char *argv[] = {"rm", filename, NULL}; // Deklarasi char -> argv, dengan argumen (rm, nama file, akhir array)
        execv("/bin/rm", argv); // Memanggil fungsi sistem yang menjalankan "/bin/rm", dengan menggunakan daftar argumen dalam argv
    }
    
    wait(&status); // Menghentikan Parent Process dan menunggu child process selesai di eksekusi serta menyimpan output t/f ke status
}

void delete_players(char *filename){ // Fungsi untuk menghapus pemain selain dari Man United

    pid_t child; // Deklarasi PID -> child
    DIR *dir; // Deklarasi DIR -> dir
    struct dirent *ent; // Deklarasi struct dirent -> ent
    char *path; // Deklarasi char -> path
    int status; // Deklarasi int -> status

    dir = opendir(filename); // Buka directory dari filename

    if (dir != NULL){ // Jika dir tidak NULL 
        int i = 0; // Deklarasi int -> i, diset 0
        while ((ent = readdir(dir))!=NULL){ // Selama masih ada entry dalam directory
            if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0){ // Jika nama file tidak mengandung"." dan "..", maka tidak diproses
                if(strstr(ent->d_name, "ManUtd") == 0){ // Jika nama file tidak mengandung "ManUtd", maka tidak diproses 
                    path = malloc(strlen(filename)+strlen(ent->d_name)+2); // Pengalokasian memori dengan ukuran yang menampung string hasil penggabungan filename dan ent->d_name, serta karakter pemisah dan null
                    sprintf(path, "%s/%s", filename, ent->d_name); // Penggabungan filename/ent->d_name, dan ditempatkan ke path
                    child = fork(); // Menyimpan PID child dengan sistem fork () 
                    if (child == 0){ // Jika child = 0
                        char *argv[] = {"rm", "-f", path, NULL}; // Deklarasi char -> argv, dengan argumen (rm, force, path, akhir array)
                        execv("/bin/rm", argv); // Memanggil fungsi sistem yang menjalankan "/bin/rm", dengan menggunakan daftar argumen dalam argv
                    }
                }
            }
        }
    }
    closedir(dir); // Menutup directory
    wait(&status); // Menghentikan Parent Process dan menunggu child process selesai di eksekusi serta menyimpan output t/f ke status
}


void categorize_players(){ // Fungsi untuk membuat folder dan pengkategoriannya

    pid_t child[4]; // Deklarasi PID -> child array
    DIR *dir; // Deklarasi DIR -> dir
    struct dirent *ent; // Deklarasi struct dirent -> ent
    char *path; //Deklarasi char -> path
    int status; // Deklarasi int -> status

    for (int i = 0; i < 4; i++){ // For loop
        char *newDir; // Deklarasi char -> newDir (Directory Baru)
        switch (i){
        case 0: // Kasus 0
            newDir = "Kiper"; // Buat directory yang bernama Kiper 
            break; // Selesai
        case 1: // Kasus 1
            newDir = "Bek"; // Buat directory yang bernama Bek
            break; // Selesai
        case 2: // Kasus 2
            newDir = "Gelandang"; // Buat directory yang bernama Gelandang
            break; // Selesai
        case 3: // Kasus 3
            newDir = "Penyerang"; // Buat directory yang bernama Penyerang
            break; // Selesai
    }
    child[i] = fork(); // Menyimpan PID child dengan sistem fork () 
    if (child[i] < 0){ // Jika child array < 0
      printf("fork failed\n"); // Print proses fork () gagal
      exit(1); // Keluar dari if statement
    }
    if (child[i] == 0){ // Jika child array = 0
      char *argv[] = {"mkdir", "-p", newDir, NULL}; // Deklarasi char -> argv, dengan argumen (mkdir, parents, newDir, akhir array)
      execv("/bin/mkdir", argv); // Memanggil fungsi sistem yang menjalankan "/bin/mkdir", dengan menggunakan daftar argumen dalam argv
    }

    while (wait(&status) > 0); // Loop untuk menunggu semua proses child selesai

    if (dir != NULL){ // Jika dir tidak NULL
      dir = opendir("players"); // Buka directory yang bernama "players"
      while ((ent = readdir(dir)) != NULL){ // Selama masih ada entry dalam directory
        if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0 ){ // Jika nama file tidak mengandung"." dan "..", maka tidak diproses
          if (ent->d_type == DT_REG && strstr(ent->d_name, newDir) != 0){ // Jika file bernilai regular dan memiliki nama seperti newDir, maka akan diproses
            path = malloc(strlen("players") + strlen(ent->d_name) + 2); // Pengalokasian memori dengan ukuran yang menampung string hasil penggabungan filename dan ent->d_name, serta karakter pemisah dan null
            sprintf(path, "%s/%s", "players", ent->d_name); // Penggabungan filename/ent->d_name, dan ditempatkan ke path
            child[i] = fork(); // Menyimpan PID child dengan sistem fork ()
            if (child[i] == 0){ // Jika child = 0
              char *argv[] = {"mv", "-f", path, newDir, NULL}; // Deklarasi char -> argv, dengan argumen (mv, force, dari path, ke newDir akhir array)
              execv("/bin/mv", argv); // Memanggil fungsi sistem yang menjalankan "/bin/mv", dengan menggunakan daftar argumen dalam argv
            }
          }
        }
      }
    }
  }
  for (int i = 0; i < 4; i++){ // For loop
    waitpid(child[i], &status, 0); // Menunggu proses child[i] sampai berakhir
  }
  closedir(dir); // Menutup directory
}


void buatTim(int a, int b, int c){ // Fungsi untuk membuat formasi (a = bek, b = gelandang, c = penyerang)

    pid_t child; // Deklarasi PID -> child
    int status; // Deklarasi int -> status

    if (child < 0) { // Jika child < 0
        printf("fork failed!\n"); // Print proses fork () gagal
        exit(1); // Keluar dari if statement
    }

    child = fork(); // Menyimpan PID child dengan sistem fork () 
    if(child == 0){ // Jika child = 0
        char pos[max_size]; // Deklarasi char -> pos dengan size max
        snprintf(pos, sizeof(pos), "ls Kiper | sort -t _ -nk4 -r | head -n 1 >> ~/Formasi_%d_%d_%d.txt", a, b, c); // Buat perintah shell (Dari folder Kiper, dengan pemisah _, urutkan kolom ke-4 secara descending, ambil yang paling atas sebanyak 1, masukkan ke dalam formasi.txt)
        execlp("sh", "sh", "-c", pos, NULL); // Eksekusi perintah shell dengan tipe string yang berada dalam argumen pos 
    }

    while ((wait(&status)) > 0); // Loop untuk menunggu semua proses child selesai
    child = fork(); // Menyimpan PID child dengan sistem fork () 
    if(child == 0){ // Jika child = 0
        char pos[max_size]; // Deklarasi char -> pos dengan size max
        snprintf(pos, sizeof(pos), "ls Bek | sort -t _ -nk4 -r | head -n %d >> ~/Formasi_%d_%d_%d.txt", a, a, b, c); // Buat perintah shell (Dari folder Bek, dengan pemisah _, urutkan kolom ke-4 secara descending, ambil yang paling atas sebanyak input a, masukkan ke dalam formasi.txt)
        execlp("sh", "sh", "-c", pos, NULL); // Eksekusi perintah shell dengan tipe string yang berada dalam argumen pos
    }

    while ((wait(&status)) > 0); // Loop untuk menunggu semua proses child selesai
    child = fork(); // Menyimpan PID child dengan sistem fork () 
    if(child == 0){ // Jika child = 0
        char pos[max_size]; // Deklarasi char -> pos dengan size max
        snprintf(pos, sizeof(pos), "ls Gelandang | sort -t _ -nk4 -r | head -n %d >> ~/Formasi_%d_%d_%d.txt", b, a, b, c); // Buat perintah shell (Dari folder Bek, dengan pemisah _, urutkan kolom ke-4 secara descending, ambil yang paling atas sebanyak input b, masukkan ke dalam formasi.txt)
        execlp("sh", "sh", "-c", pos, NULL); // Eksekusi perintah shell dengan tipe string yang berada dalam argumen pos
    }

    while ((wait(&status)) > 0); // Loop untuk menunggu semua proses child selesai
    child = fork(); // Menyimpan PID child dengan sistem fork () 
    if(child == 0){ // Jika child = 0
        char pos[max_size]; // Deklarasi char -> pos dengan size max
        snprintf(pos, sizeof(pos), "ls Penyerang | sort -t _ -nk4 -r | head -n %d >> ~/Formasi_%d_%d_%d.txt", c, a, b, c); // Buat perintah shell (Dari folder Bek, dengan pemisah _, urutkan kolom ke-4 secara descending, ambil yang paling atas sebanyak input c, masukkan ke dalam formasi.txt)
        execlp("sh", "sh", "-c", pos, NULL); // Eksekusi perintah shell dengan tipe string yang berada dalam argumen pos
    }
    
    while ((wait(&status)) > 0); // Loop untuk menunggu semua proses child selesai
}


int main() { // Driver Code

    downloadDB("https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", "players.zip");
    unzip("players.zip");
    rm("players.zip");
    delete_players("players");
    categorize_players();
    buatTim(3,4,3);


    return 0;
}
