# Modul 2

## Anggota

- Irsyad Fikriansyah Ramadhan (5025211149)
- Ligar Arsa Arnata (5025211244)
- Radhiyan Muhammad Hisan (5025211166)

## Penjelasan

### soal1

![Gambar soal no 1](gambar/1.jpg)

- Pada bagian awal fungsi main, dideklarasikan beberapa variabel beserta isinya
- kemudian memanggil fungsi `download(char url[], char filename[])` untuk mengunduh file zip dari link yang telah deklarasikan di awal
- setelah berhasil mengunduh file zip, kemudian dilakyukan unzip dengan memanggil fungsi `unzip(char filename[])`
- setelah di-unzip, file zip kemudian akan dihapus menggunakan fungsi `rm(char filename[])`
- kemudian membuat directory sebanyak jenis hewan menggunakan fungsi `make_directory(char dirname[])`
- kemudian dilakukan pengelist an seluruh file yang memiliki ekstensi `.jpg` pada current directory menggunakan fungsi `ls_grep(char file_list[][max_size], char extension[])`
- keluaran dari fungsi `ls_grep()` adalah banyaknya file yang sesuai dengan grep sebagai integer
- setelah mendapatkan list file dengan ekstensi `.jpg` dan banyaknya, kemudian dilakukan pemilihan random untuk menentukan hewan apa yang akan dijaga oleh Grape-kun
- setelah didapatkan index random menggunakan fungsi bawaan c `rand()`, kemudian print nama file dengan index random tersebut tanpa adanya `_*.jpg`. hal ini dapat didapatkan menggunakan fungsi `cut_until`
- kemudian dilakukan looping sebanyak jenis hewan untunk mengelist jenis hewan dan memindahkannya ke masing-masing folder menggunakan fungsi `mv(char filename[][max_size], char dirname[], int size)`
- setelah semua dipindahkan, list semua folder dengan awalan "Hewan" dan kemudian zip folder-folder tersebut menggunakan fungsi `zip(char file_list[][max_size], char filename[], int file_count)`

### soal2

![Gambar soal no 2a](gambar/2ai.png)![Gambar soal no 2a](gambar/2aii.png)

**a.** Program membuat folder menggunakan fungsi `buat_dir()` dengan nama _timestamp_ saat program dijalankan. Dijalankan tiap 30 detik menggunakan `sleep(30)` di dalam loop `while(1)`. Fungsi `buat_dir()` membuat _child process_ untuk membuat folder.

![Gambar soal no 2b](gambar/2bi.png)![Gambar soal no 2b](gambar/2bii.png)

**b.** Setelah membuat folder, fungsi `buat_dir()` membuat _child process_ lagi untuk menjalankan loop untuk memanggil fungsi `unduh_gambar()` untuk mengunduh gambar tiap 5 detik menggunakan `sleep(5)` di dalam loop dan dihitung iterasinya. Gambar diunduh dari tautan yang telah ditambahkan ukuran gambar dari perhitungan aritmatika waktu epoch unix. Nama file gambar adalah _timestamp_ saat fungsi dipanggil.

![Gambar soal no 2c](gambar/2c.png)

**c.** Saat iterasi pengunduhan gambar telah mencapai 15, fungsi `buat_dir()` memanggil fungsi `kompres()` untuk men-_zip_ folder.

![Gambar soal no 2d](gambar/2di.png)![Gambar soal no 2d](gambar/2dii.png)![Gambar soal no 2d](gambar/2diii.png)

**d.** Program "killer" dibuat menggunakan fungsi `buat_file_kill()` dan dijadikan _executable (exe)_ menggunakan fungsi `exe_file_kill()`.

![Gambar soal no 2e](gambar/2e.png)

**e.** Fungsi utama akan mengecek apakah ada argumen yang diberikan. Jika tidak, maka program tidak dijalankan. Jika ada, maka akan dikirim ke fungsi `buat_file_kill()` yang kemudian akan dibuat kode sesuai kebutuhan. MODE_A dengan argumen `-a` menggunakan `pkill` dan pola nama _exe_ untuk di-_kill_. MODE_B dengan argumen `-b` menggunakan fungsi bawaan `kill()` untuk mengirim sinyal `SIGKILL` ke _process id_ yang diberikan.

**<i>Catatan:</i>** Kebanyakan fungsi menggunakan templat pada modul 2 (mem-_fork child process_ dan menggunakan `execv()` untuk menjalankan _command shell_)

### soal3

![Gambar soal no 3](gambar/3a.png)
![Gambar soal no 3](gambar/3aa.png)

A.Saat program pertama kali berjalan, program akan mendownload file dari link drive yang disediakan, proses tersebut akan dijalankan oleh fungsi “downloadDB”, kemudian program akan melakukan unzip atau ekstrak dari file yang didownload menggunakan fungsi “unzip”. Ketika sudah diunzip, maka file zip awalnya akan dihapus menggunakan fungsi “rm”.

![Gambar soal no 3b](gambar/3b.png)

B.Setelah menjalankan proses A, program akan menjalankan fungsi “delete_players” yang mana akan menghapus file seluruh pemain yang bukan dari ManUtd dengan melalui proses string compare dari nama file dan string “ManUtd” serta penghapusan path dari file tersebut.

![Gambar soal no 3c](gambar/3c.png)

C.Setelah file difilter berdasarkan nama tim, File yang tersisa akan dikategorikan kedalam folder baru sesuai dengan posisinya. Proses tersebut menggunakan fungsi “categorize_players”, dimana fungsi tersebut akan membuat folder baru dengan menggunakan switch case dan mkdir, dan akan mengkategorikannya dengan melakukan string compare antara nama file dengan nama folder baru sehingga yang sesuai akan di move ke folder tersebut.

![Gambar soal no 3d](gambar/3d.png)

D.Langkah terakhir setelah file dikategorikan, akan ada fungsi bernama “buatTim” yang akan mengeluarkan output berupa file txt dan formasi yang diinginkan. File txt itu sendiri didalamnya berisikan daftar nama pemain dengan rating yang paling tinggi. Nama-nama pemain tersebut didapatkan dari eksekusi perintah shell yang akan mengurutkan rating setiap file dalam folder dan mengambil jumlahnya sesuai angka yang diinputkan pengguna.

### soal4

![Gambar soal no 4](gambar/4.jpg)

- setelah program mainan.c dicompile, program akan meminta argumen sebanyak 5 yaitu nama output compile, jam, menit, detik, dan lokasi bash script.
- jika telah diberikan 5 argumen, maka akan dilakukan pengecekan terhadap argumen yang diberikan dengan fungsi `check_argv(char str[], int code)`
- pengecekan akan dilakukan dengan code:
  - 0 untuk jam
  - 1 untuk menit
  - 2 untuk detik
  - 3 untuk lokasi bash script
- keluaran dari fungsi `check argv` adalah integer, dimana jika code sama dengan 1 hingga 2 akan mereturn str sebagai integer itu sendiri jika valid, -1 jika argumen berupa '*', dan `print_error(0)` jika argumen tidak valid
- untuk pengecekan lokasi bash script sendiri ada beberapa step yang dilakukan:
  - string dibagi menjadi 2, directory dan filename
  - pembagian string dapat menggunakan cut terhadap '/' terakhir
  - jika string tidak memiliki '/' maka string tersebut merupakan filename dan directory merupakan '.'
  - setelah dibagi, kemudian melakukan pengecekan terhadap directory
  - jika directory tidak ditemukan, maka panggil `print_error(2)`
  - jika ditemukan, kemudian dilakukan pengecekan ada tidaknya file pada directory tersebut
  - jika ditemukan maka return 1 (tidak digunakan), jika tidak, maka `print_error(1)`
- setelah dilakukan pengecekan terhadap argumen, maka perlu melakukan `fork()` dan kill parent process agar menjadi deamon
- pada loop utama, dilakukan pengecekan waktu sekarang dan pengecekan terhadap argumen, jika sama maka jalankan bash script sesuai lokasi yang ada di argumen
- pada loop utama, dilakukan `sleep(1)` agar penggunaan cpu rendah
